﻿using System.Linq;
using NaughtierAttributes;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BuildingsSpawner : MonoBehaviour
{
    [SerializeField, Hide, FindObject] private ScoreController _scoreController;

    [SerializeField, Hide, GetComponentsInChildren]
    private BuildingSpawn[] _spawns;

    [SerializeField] private AnimationCurve _spawnDelay;

    private float m_spawnTimer;

    private void Update()
    {
        m_spawnTimer -= Time.deltaTime;
        if (m_spawnTimer <= 0)
        {
            Spawn();
            m_spawnTimer = _spawnDelay.Evaluate(_scoreController.GetScore());
        }
    }

    private void Spawn()
    {
        if (_spawns.All(spawn => spawn.AreAllSpawned))
        {
            SceneManager.LoadScene(0);
            return;
        }

        var spawned = false;
        while (!spawned)
        {
            var index = Random.Range(0, _spawns.Length);
            spawned = _spawns[index].Spawn();
        }
    }

    public void SetCollidersAsTrigger(bool asTrigger)
    {
        foreach (var spawn in _spawns) spawn.SetCollidersAsTrigger(asTrigger);
    }

    public void DropBomb(Vector2 position)
    {
        foreach (var spawn in _spawns)
        {
            if (!spawn.Contains(position.x)) continue;
            spawn.DropBomb(position.y);
            return;
        }
    }
}