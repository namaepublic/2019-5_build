﻿using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TimerController : MonoBehaviour
{
    [SerializeField] private float _totalTime;
    [SerializeField] private TextMeshProUGUI _timerText;
    [SerializeField] private Image _timerFill;
    [SerializeField] private UnityEvent _event;

    private Button _button;

    private float _timeLeft;
    private bool _init;

    public float TimeLeft
    {
        get { return _timeLeft; }
        set
        {
            _timeLeft = value;
            var roundTime = Mathf.RoundToInt(_timeLeft);
            var minutes = roundTime / 60;
            var seconds = roundTime - minutes * 60;
            _timerText.text = minutes.ToString("00") + ":" + seconds.ToString("00");
            _timerFill.fillAmount = _timeLeft / _totalTime;

            if (_timeLeft <= 0 && _event != null)
                _event.Invoke();
        }
    }

    public void Init()
    {
        _init = true;
        TimeLeft = _totalTime;
    }

    private void Update()
    {
        if (!_init) return;
        TimeLeft -= Time.deltaTime;
    }
}