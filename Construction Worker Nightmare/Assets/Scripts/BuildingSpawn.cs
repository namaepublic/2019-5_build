﻿using System;
using System.Linq;
using NaughtierAttributes;
using UnityEngine;

public class BuildingSpawn : MonoBehaviour
{
    [SerializeField, Hide, GetComponentsInChildren]
    private BuildingController[] _buildings;

    [SerializeField] private Vector2 _xMinMax;

    public void SetCollidersAsTrigger(bool asTrigger)
    {
        foreach (var building in _buildings) building.SetColliderAsTrigger(asTrigger);
    }

    public bool Spawn()
    {
        foreach (var buildingController in _buildings)
        {
            if (!buildingController.NotStarted) continue;
            buildingController.Spawn();
            return true;
        }
        return false;
    }

    public bool AreAllSpawned => _buildings.All(building => !building.NotStarted);

    public bool Contains(float x)
    {
        var xPos = transform.position.x;
        return x >= xPos + _xMinMax.x && x <= xPos + _xMinMax.y;
    }

    public void DropBomb(float yPos)
    {
        foreach (var building in _buildings)
        {
            if (!building.Contains(yPos)) continue;
            building.DropBomb();
            return;
        }
    }

#if UNITY_EDITOR
    private void OnValidate()
    {
        for (var i = 0; i < _buildings.Length; i++) _buildings[i].index = i;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        var position = transform.position;
        Gizmos.DrawWireSphere(new Vector2(position.x + _xMinMax.x, position.y), .2f);
        Gizmos.DrawWireSphere(new Vector2(position.x + _xMinMax.y, position.y), .2f);
    }
#endif

    public void RemoveBuilding(int index)
    {
        if (index >= _buildings.Length) return;
        if (index == _buildings.Length - 1)
        {
            _buildings[index].SetState(BuildingState.NotStarted);
            return;
        }
        _buildings[index].InheritState(_buildings[index + 1]);
        RemoveBuilding(index + 1);
    }
}