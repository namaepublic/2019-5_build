﻿using System;
using System.Linq;
using NaughtierAttributes;
using UnityEngine;

[Flags]
public enum Intent : byte
{
    Left = 0b1,
    Right = 0b10,
    Jump = 0b100,
    Dive = 0b1000
}

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour
{
    [Header("Components")] [SerializeField]
    private Transform _graphicsTransform;

    [SerializeField, Hide, GetComponent] private Rigidbody2D _rigidbody2D;
    [SerializeField, Hide, FindObject] private BuildingsSpawner _buildingsSpawner;

    [Header("Keys")] [SerializeField] private KeyCode[] _leftKeys;
    [SerializeField] private KeyCode[] _rightKeys;
    [SerializeField] private KeyCode[] _jumpKeys;
    [SerializeField] private KeyCode[] _diveKeys;
    [SerializeField] private KeyCode[] _bombKeys;

    [Header("Movement stats")] [SerializeField]
    private float _groundedSpeed;

    [SerializeField] private float _midAirSpeed;

    [Header("Jump stats")] [SerializeField]
    private float _jumpForce;

    [SerializeField] private int _jumpAmount;
    [SerializeField] private float _multipleJumpDelay;
    [SerializeField] private float _groundCheckRadius;
    [SerializeField] private Vector2 _groundCheckOffset;
    [SerializeField] private LayerMask _groundLayerMask;

    [Header("Dive stats")] [SerializeField]
    private LayerMask _buildingLayerMask;

    [SerializeField] private float _diveForce;

    private Intent m_intent;

    private bool m_isGrounded;

    private int m_jumpCount;
    private float m_multipleJumpCooldown;

    private bool m_overBuilding;
    private bool m_dropping;

    public void SetJumpAmount(int value) => _jumpAmount = value;

    private void Update()
    {
        HandleIntents();
        HandleTimers(Time.deltaTime);
    }

    private void FixedUpdate()
    {
        if (m_isGrounded = CheckGround()) m_jumpCount = _jumpAmount;
        if (!(m_overBuilding = CheckOverBuilding()) && m_dropping)
        {
            m_dropping = false;
            _buildingsSpawner.SetCollidersAsTrigger(false);
        }

        var deltaTime = Time.deltaTime;
        var deltaSpeed = deltaTime * (m_isGrounded ? _groundedSpeed : _midAirSpeed);

        if (HasIntent(Intent.Left))
        {
            FlipX(true);
            _rigidbody2D.position += deltaSpeed * Vector2.left;
        }

        if (HasIntent(Intent.Right))
        {
            FlipX(false);
            _rigidbody2D.position += deltaSpeed * Vector2.right;
        }

        if (HasIntent(Intent.Jump))
        {
            if (m_jumpCount > 0 && m_multipleJumpCooldown <= 0)
            {
                m_multipleJumpCooldown = _multipleJumpDelay;
                _rigidbody2D.velocity = _jumpForce * Vector2.up;
                m_jumpCount--;
            }

            RemoveIntent(Intent.Jump);
        }

        if (HasIntent(Intent.Dive))
        {
            if (m_overBuilding)
            {
                m_dropping = true;
                _buildingsSpawner.SetCollidersAsTrigger(true);
                _rigidbody2D.velocity = _diveForce * Vector2.down;
            }
            else if (!m_isGrounded)
                _rigidbody2D.velocity = _diveForce * Vector2.down;

            RemoveIntent(Intent.Dive);
        }
    }

    private void HandleIntents()
    {
        if (CheckKeysDown(_leftKeys))
        {
            RemoveIntent(Intent.Right);
            AddIntent(Intent.Left);
        }

        if (CheckKeysDown(_rightKeys))
        {
            RemoveIntent(Intent.Left);
            AddIntent(Intent.Right);
        }

        if (CheckKeysUp(_leftKeys) && !CheckKeys(_leftKeys))
        {
            RemoveIntent(Intent.Left);
            CheckAndAddIntent(_rightKeys, Intent.Right);
        }

        if (CheckKeysUp(_rightKeys) && !CheckKeys(_rightKeys))
        {
            RemoveIntent(Intent.Right);
            CheckAndAddIntent(_leftKeys, Intent.Left);
        }

        CheckDownAndAddIntent(_jumpKeys, Intent.Jump);
        CheckDownAndAddIntent(_diveKeys, Intent.Dive);

        if (m_overBuilding && CheckKeysDown(_bombKeys))
        {
            _buildingsSpawner.DropBomb(transform.position);
        }
    }

    private void HandleTimers(float deltaTime)
    {
        m_multipleJumpCooldown = m_isGrounded ? 0 : m_multipleJumpCooldown - deltaTime;
    }

    public void FlipX() => FlipX(transform.localScale.x > 0);

    public void FlipX(bool faceLeft)
    {
        var scale = _graphicsTransform.localScale;
        scale.x = faceLeft ? -Mathf.Abs(scale.x) : Mathf.Abs(scale.x);
        _graphicsTransform.localScale = scale;
    }

    private bool CheckOverBuilding() => _rigidbody2D.IsTouchingLayers(_buildingLayerMask);

    private bool CheckGround() =>
        Physics2D.OverlapCircle((Vector2) transform.position + _groundCheckOffset, _groundCheckRadius, _groundLayerMask);

    private static bool CheckKeys(KeyCode[] keys) => keys.Any(key => Input.GetKey(key));

    private static bool CheckKeysDown(KeyCode[] keys) => keys.Any(key => Input.GetKeyDown(key));

    private static bool CheckKeysUp(KeyCode[] keys) => keys.Any(key => Input.GetKeyUp(key));

    private bool HasIntent(Intent intent) => (m_intent & intent) != 0;

    private void AddIntent(Intent intent) => m_intent |= intent;

    private void RemoveIntent(Intent intent) => m_intent &= ~intent;

    private void IsolateIntent(Intent intent) => m_intent &= intent;

    private bool CheckAndAddIntent(KeyCode[] keys, Intent intent)
    {
        if (!CheckKeys(keys)) return false;
        AddIntent(intent);
        return true;
    }

    private bool CheckDownAndAddIntent(KeyCode[] keys, Intent intent)
    {
        if (!CheckKeysDown(keys)) return false;
        AddIntent(intent);
        return true;
    }

#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(_rigidbody2D.position + _groundCheckOffset, _groundCheckRadius);
    }
#endif
}