﻿using System;
using NaughtierAttributes;
using UnityEngine;
using UnityEngine.UI;

public enum BuildingState
{
    NotStarted,
    Starting,
    Constructing,
    Finished
}

public class BuildingController : MonoBehaviour
{
    [ReadOnly] public int index;

    [SerializeField, Hide, ValueFrom("InitSpawn")]
    private BuildingSpawn _spawn;

    [SerializeField, Hide, FindObject] private ScoreController _scoreController;
    [SerializeField] private Image _timerFill;
    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private BoxCollider2D _collider;
    [SerializeField] private GameObject _workers;
    [SerializeField] private Sprite _startingSprite, _constructingSprite, _finishedSprite;

    [SerializeField] private float _startingDuration, _constructingDuration;
    [SerializeField] private Color _startingFillColor, _constructingFillColor;

    [SerializeField] private Vector2 _yMinMax;
    [SerializeField] private DynamiteController _dynamite;

    private BuildingState m_state;
    private float m_stateTimer;
    private bool m_willExplode;

    public bool NotStarted => m_state == BuildingState.NotStarted;
    public bool Finished => m_state == BuildingState.Finished;
    public bool UnderConstruction => m_state != BuildingState.NotStarted && m_state != BuildingState.Finished;

    private void Start() => SetState(BuildingState.NotStarted);

    private void Update()
    {
        if (!UnderConstruction || m_willExplode) return;
        m_stateTimer -= Time.deltaTime;
        _timerFill.fillAmount = 1 - m_stateTimer / GetDuration();
        if (m_stateTimer <= 0) Upgrade();
    }

    public void InheritState(BuildingController building)
    {
        SetState(building.m_state);
        if (!UnderConstruction) return;
        _dynamite.ResetBomb();
        m_stateTimer = building.m_stateTimer;
        if (m_willExplode = building.m_willExplode) _dynamite.InheritBomb(building._dynamite);
    }

    public void SetColliderAsTrigger(bool asTrigger)
    {
        if (!NotStarted) _collider.isTrigger = asTrigger;
    }

    public void Spawn() => SetState(BuildingState.Starting);

    public void Upgrade()
    {
        if (UnderConstruction) SetState(m_state + 1);
    }

    public void SetState(BuildingState state)
    {
        switch (m_state = state)
        {
            case BuildingState.NotStarted:
                _spriteRenderer.enabled = false;

                _workers.SetActive(false);
                _collider.enabled = false;

                _dynamite.ResetBomb();
                m_willExplode = false;
                break;
            case BuildingState.Starting:
                _spriteRenderer.enabled = true;
                _spriteRenderer.sprite = _startingSprite;
                _workers.SetActive(true);

                _collider.enabled = true;

                m_stateTimer = _startingDuration;
                _timerFill.color = _startingFillColor;
                break;
            case BuildingState.Constructing:
                _spriteRenderer.enabled = true;
                _spriteRenderer.sprite = _constructingSprite;
                _workers.SetActive(true);

                _collider.enabled = true;

                m_stateTimer = _constructingDuration;
                _timerFill.color = _constructingFillColor;
                break;
            case BuildingState.Finished:
                _workers.SetActive(false);

                _spriteRenderer.sprite = _finishedSprite;

                _collider.enabled = true;
                break;
        }
    }

    public bool Contains(float y)
    {
        var yPos = transform.position.y;
        return y >= yPos + _yMinMax.x && y <= yPos + _yMinMax.y;
    }

    public void DropBomb()
    {
        if (!UnderConstruction) return;
        m_willExplode = true;
        _dynamite.DropBomb();
    }

    public void Destroy()
    {
        _scoreController.IncreaseScore();
        _spawn.RemoveBuilding(index);
    }

    private float GetDuration()
    {
        switch (m_state)
        {
            case BuildingState.Starting:
                return _startingDuration;
            case BuildingState.Constructing:
                return _constructingDuration;
        }

        return 0;
    }

#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        var position = transform.position;
        Gizmos.DrawWireSphere(new Vector2(position.x, position.y + _yMinMax.x), .2f);
        Gizmos.DrawWireSphere(new Vector2(position.x, position.y + _yMinMax.y), .2f);
    }

    private BuildingSpawn InitSpawn()
    {
        var parent = transform.parent;
        return parent != null ? parent.GetComponent<BuildingSpawn>() : null;
    }
#endif
}