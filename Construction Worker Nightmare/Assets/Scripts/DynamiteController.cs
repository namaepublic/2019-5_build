﻿using UnityEngine;

public class DynamiteController : MonoBehaviour
{
    [SerializeField] private BuildingController _buildingController;
    [SerializeField] private Animator _animator;

    public void Explode() => _buildingController.Destroy();

    public void InheritBomb(DynamiteController dynamite)
    {
        DropBomb();
        _animator.playbackTime = dynamite._animator.playbackTime;
    }
    
    public void DropBomb() => gameObject.SetActive(true);

    public void ResetBomb() => gameObject.SetActive(false);
}