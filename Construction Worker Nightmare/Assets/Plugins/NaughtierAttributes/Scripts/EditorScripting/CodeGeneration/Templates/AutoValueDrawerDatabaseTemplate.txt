﻿#if UNITY_EDITOR
// This class is auto generated

using System;
using System.Collections.Generic;

namespace NaughtyAttributes.Editor
{
    public static class __classname__
    {
        private static Dictionary<Type, BaseAutoValueDrawer> drawersByAttributeType;

        static __classname__()
        {
            drawersByAttributeType = new Dictionary<Type, BaseAutoValueDrawer>();
            __entries__
        }

        public static BaseAutoValueDrawer GetDrawerForAttribute(Type attributeType)
        {
            BaseAutoValueDrawer drawer;
            if (drawersByAttributeType.TryGetValue(attributeType, out drawer))
            {
                return drawer;
            }
            else
            {
                return null;
            }
        }

        public static void ClearCache()
        {
            foreach (var kvp in drawersByAttributeType)
            {
                kvp.Value.ClearCache();
            }
        }
    }
}
#endif