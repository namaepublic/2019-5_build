#if UNITY_EDITOR
using UnityEditor;

namespace NaughtierAttributes.Editor
{
    [PropertyDrawCondition(typeof(ShowIfAttribute))]
    public class ShowIfPropertyDrawCondition : BasePropertyDrawCondition
    {
        public override bool CanDrawProperty(SerializedProperty property)
        {
            ShowIfAttribute showIfAttribute = PropertyUtility.GetAttribute<ShowIfAttribute>(property);
            var target = PropertyUtility.GetTargetObject(property);
            
            var canDraw = true;
            foreach (var conditionName in showIfAttribute.ConditionNames)
            {
                if (SerializedPropertyUtility.GetValueFromTypeInfo(target, conditionName, out bool temp))
                    canDraw = canDraw && temp;
                else
                {
                    string warning = showIfAttribute.GetType().Name + " needs a valid boolean condition field or method name to work";
                    EditorDrawUtility.DrawHelpBox(warning, MessageType.Warning, logToConsole: true, context: target);

                    return true; 
                }
            }
            return canDraw;
        }
    }
}
#endif